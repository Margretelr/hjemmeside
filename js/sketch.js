let minCanvas;
let r=0;
let speed=2;
function setup(){
    minCanvas = createCanvas(windowWidth, windowHeight);
    minCanvas.parent("p5Canvas");
    angleMode(DEGREES);
}

function draw(){  
    clear();
    fill(255, 200, 0);
    noStroke();

    translate(mouseX, mouseY);
    push();
    rotate(r);
    r+=speed;
    textSize(120);
    text("*", 0, 0)
    pop();

    if (mouseIsPressed){
        speed=10;
    }else{
        speed=2;
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

